using Microsoft.AspNetCore.Authentication;

namespace TestLogin.Services
{
    public interface ITokenService
    {
        string GetToken(int userId);
        string GetRefreshToken(int userId);

        AuthenticateResult ValidateRefreshToken(string refreshToken);
    }
}