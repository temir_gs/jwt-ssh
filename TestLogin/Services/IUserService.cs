using TestLogin.Models;

namespace TestLogin.Services
{
    public interface IUserService
    {
        bool ValidateCredentials(UserCredentials userCredentials);
        User GetUser(string username);
    }
}