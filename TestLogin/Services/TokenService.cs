using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;
using TestLogin.Certificates;
using TestLogin.Helpers;
using TestLogin.Redis;

namespace TestLogin.Services
{
    public class TokenService : ITokenService
    {
        private readonly SigningAudienceCertificate signingAudienceCertificate;
        private readonly SigningIssuerCertificate signingIssuerCertificate;
        private readonly ICacheHelper cacheHelper;

        public TokenService(ICacheHelper cacheHelper, SigningIssuerCertificate signingIssuerCertificate)
        {
            this.cacheHelper = cacheHelper;
            this.signingIssuerCertificate = signingIssuerCertificate;
            signingAudienceCertificate = new SigningAudienceCertificate();
        }

        public string GetToken(int userId)
        {
            IList<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, userId.ToString()),
            };

            //Redis
            cacheHelper.SetAccessTokenPublicKey(userId, signingAudienceCertificate.GetAudienceSigningKey().Item2);

            string token = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(
                signingCredentials: signingAudienceCertificate.GetAudienceSigningKey().Item1,
                expires: DateTime.UtcNow.AddDays(7),
                claims: claims
            ));

            return token;
        }

        public string GetRefreshToken(int userId)
        {
            IList<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, userId.ToString()),
            };

            //Redis
            cacheHelper.SetRefreshTokenPublicKey(userId, signingAudienceCertificate.GetAudienceSigningKey().Item2);

            string refreshToken = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(
                expires: DateTime.Now.AddYears(1),
                signingCredentials: signingAudienceCertificate.GetAudienceSigningKey().Item1,
                claims: claims
            ));

            return refreshToken;
        }

        public AuthenticateResult ValidateRefreshToken(string refreshToken)
        {
            try
            {
                //Read RefreshTokenPublicKey from Redis
                RsaSecurityKey issuerSigningKey =
                    signingIssuerCertificate.GetIssuerRefreshTokenSigningKey(AuthHelper.GetUserId(refreshToken));
                return AuthHelper.ValidateToken(refreshToken, issuerSigningKey, null);
            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail(ex.Message);
            }
        }
    }
}