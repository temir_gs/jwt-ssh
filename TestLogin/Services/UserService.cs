using System.Collections.Generic;
using System.Linq;
using TestLogin.Models;

namespace TestLogin.Services
{
    public class UserService : IUserService
    {
        public bool ValidateCredentials(UserCredentials userCredentials)
        {
            User user = GetUser(userCredentials.Username);
            bool isValid = user != null && AreValidCredentials(userCredentials, user);
            return isValid;
        }

        public User GetUser(string username)
        {
            IEnumerable<User> users = new List<User>
            {
                new User
                {
                    Username = "temirg",
                    Password = "12345"
                }
            };
            return users.SingleOrDefault(u => u.Username.Equals(username));
        }

        private static bool AreValidCredentials(UserCredentials userCredentials, User user)
        {
            return user.Username == userCredentials.Username &&
                   user.Password == userCredentials.Password;
        }
    }
}