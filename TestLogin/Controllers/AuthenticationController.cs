using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestLogin.Models;
using TestLogin.Services;

namespace TestLogin.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly ITokenService tokenService;

        public AuthenticationController(IUserService userService,
            ITokenService tokenService)
        {
            this.userService = userService;
            this.tokenService = tokenService;
        }

        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] UserCredentials userCredentials)
        {
            if (userService.ValidateCredentials(userCredentials))
            {
                return Ok(new UserToken
                {
                    AccessToken = tokenService.GetToken(1),
                    RefreshToken = tokenService.GetRefreshToken(1)
                });
            }

            await Task.CompletedTask;
            return BadRequest("UserName or Password is invalid!");
        }

        [HttpGet]
        public async Task<IActionResult> RefreshSignInAsync([FromQuery] string refreshToken)
        {
            if (tokenService.ValidateRefreshToken(refreshToken).Succeeded)
            {
                return Ok(new UserToken
                {
                    AccessToken = tokenService.GetToken(1),
                    RefreshToken = tokenService.GetRefreshToken(1)
                });
            }

            await Task.CompletedTask;
            return Unauthorized();
        }
    }
}