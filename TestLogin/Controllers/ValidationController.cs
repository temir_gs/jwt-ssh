using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TestLogin.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    public class ValidationController : Controller
    {

        [Authorize]
        [HttpGet]
        public IActionResult Validate()
        {
            return Ok();
        }
    }
}