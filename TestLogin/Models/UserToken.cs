namespace TestLogin.Models
{
    public class UserToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}