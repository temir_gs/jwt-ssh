using System.Threading.Tasks;

namespace TestLogin.Redis
{
    public interface ICacheHelper
    {
        void SetAccessTokenPublicKey(int userId, string publicKey);
        string GetAccessTokenPublicKey(int userId);

        void SetRefreshTokenPublicKey(int userId, string publicKey);
        string GetRefreshTokenPublicKey(int userId);
    }
}