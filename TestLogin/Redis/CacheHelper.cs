using System;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;

namespace TestLogin.Redis
{
    public class CacheHelper : ICacheHelper
    {
        private readonly IDistributedCache distributedCache;

        public CacheHelper(IDistributedCache distributedCache)
        {
            this.distributedCache = distributedCache;
        }

        public void SetAccessTokenPublicKey(int userId, string publicKey)
        {
            string jsonData = JsonSerializer.Serialize(publicKey);
            string key = $"test:publicKey:accessToken:{userId}";
            byte[] value = Encoding.UTF8.GetBytes(jsonData);
            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromDays(30));

            distributedCache.Set(key, value, options);
        }

        public string GetAccessTokenPublicKey(int userId)
        {
            string key = $"test:publicKey:accessToken:{userId}";

            byte[] encodedUserPublicKey = distributedCache.Get(key);

            return encodedUserPublicKey == null
                ? null
                : JsonSerializer.Deserialize<string>(Encoding.UTF8.GetString(encodedUserPublicKey));
        }

        public void SetRefreshTokenPublicKey(int userId, string publicKey)
        {
            string jsonData = JsonSerializer.Serialize(publicKey);
            string key = $"test:publicKey:refreshToken:{userId}";
            byte[] value = Encoding.UTF8.GetBytes(jsonData);
            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromDays(30));

            distributedCache.Set(key, value, options);
        }

        public string GetRefreshTokenPublicKey(int userId)
        {
            string key = $"test:publicKey:refreshToken:{userId}";

            byte[] encodedUserPublicKey = distributedCache.Get(key);

            return encodedUserPublicKey == null
                ? null
                : JsonSerializer.Deserialize<string>(Encoding.UTF8.GetString(encodedUserPublicKey));
        }
    }
}