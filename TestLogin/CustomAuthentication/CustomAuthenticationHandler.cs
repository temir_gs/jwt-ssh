using System;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using TestLogin.Certificates;
using TestLogin.Helpers;

namespace TestLogin.CustomAuthentication
{
    public class CustomAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        private readonly SigningIssuerCertificate signingIssuerCertificate;

        public CustomAuthenticationHandler(IOptionsMonitor<BasicAuthenticationOptions> options, ILoggerFactory logger,
            UrlEncoder encoder, ISystemClock clock, SigningIssuerCertificate signingIssuerCertificate) : base(options,
            logger, encoder, clock)
        {
            this.signingIssuerCertificate = signingIssuerCertificate;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Unauthorized");

            string authorizationHeader = Request.Headers["Authorization"];
            if (string.IsNullOrEmpty(authorizationHeader))
                return AuthenticateResult.NoResult();

            if (!authorizationHeader.StartsWith("bearer", StringComparison.OrdinalIgnoreCase))
                return AuthenticateResult.Fail("Unauthorized");

            string token = authorizationHeader.Substring("bearer".Length).Trim();
            if (string.IsNullOrEmpty(token))
                return AuthenticateResult.Fail("Unauthorized");

            try
            {
                await Task.CompletedTask;
                //Read AccessTokenPublicKey from Redis
                RsaSecurityKey issuerSigningKey =
                    signingIssuerCertificate.GetIssuerAccessTokenSigningKey(AuthHelper.GetUserId(token));
                return AuthHelper.ValidateToken(token, issuerSigningKey, Scheme.Name);
            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail(ex.Message);
            }
        }
    }
}