using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;

namespace TestLogin.Helpers
{
    public static class AuthHelper
    {
        public static int GetUserId(string token)
        {
            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            if (!(jwtSecurityTokenHandler.ReadToken(token) is JwtSecurityToken jwtSecurity))
                throw new AggregateException("Token is invalid!");

            int userId = int.Parse(jwtSecurity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid)?.Value ??
                                   string.Empty);
            return userId;
        }

        public static AuthenticateResult ValidateToken(string token, RsaSecurityKey issuerSigningKey, string schemaName)
        {
            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();

            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = issuerSigningKey,
                LifetimeValidator = LifetimeValidator
            };
            ClaimsPrincipal principal =
                jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters,
                    out _);

            AuthenticationTicket ticket = new AuthenticationTicket(principal, schemaName);
            return AuthenticateResult.Success(ticket);
        }
        
        private static bool LifetimeValidator(DateTime? notBefore,
            DateTime? expires,
            SecurityToken securityToken,
            TokenValidationParameters validationParameters)
        {
            return expires != null && expires > DateTime.UtcNow;
        }
    }
}