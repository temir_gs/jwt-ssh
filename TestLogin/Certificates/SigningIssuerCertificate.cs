using System;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using TestLogin.Redis;

namespace TestLogin.Certificates
{
    public class SigningIssuerCertificate : IDisposable
    {
        private readonly RSA rsa;
        private readonly ICacheHelper cacheHelper;

        public SigningIssuerCertificate(ICacheHelper cacheHelper)
        {
            this.cacheHelper = cacheHelper;
            rsa = RSA.Create();
        }

        public RsaSecurityKey GetIssuerAccessTokenSigningKey(int userId)
        {
            rsa.FromXmlString(cacheHelper.GetAccessTokenPublicKey(userId));
            return new RsaSecurityKey(rsa);
        }

        public RsaSecurityKey GetIssuerRefreshTokenSigningKey(int userId)
        {
            rsa.FromXmlString(cacheHelper.GetRefreshTokenPublicKey(userId));
            return new RsaSecurityKey(rsa);
        }

        public void Dispose()
        {
            rsa?.Dispose();
        }
    }
}