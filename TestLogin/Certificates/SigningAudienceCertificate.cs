using System;
using System.IO;
using System.Security.Cryptography;
using System.Xml.Serialization;
using Microsoft.IdentityModel.Tokens;

namespace TestLogin.Certificates
{
    public class SigningAudienceCertificate : IDisposable
    {
        private readonly RSA rsa;
        static RSACryptoServiceProvider cryptoService;
        private readonly RSAParameters privateKey;
        private readonly RSAParameters publicKey;

        public SigningAudienceCertificate()
        {
            rsa = RSA.Create();
            cryptoService = new RSACryptoServiceProvider(2048);
            privateKey = cryptoService.ExportParameters(true);
            publicKey = cryptoService.ExportParameters(false);
        }

        private string GetPublicKeyString()
        {
            StringWriter sw = new StringWriter();
            XmlSerializer xs = new XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, publicKey);
            return sw.ToString();
        }

        private string GetPrivateKeyString()
        {
            StringWriter sw = new StringWriter();
            XmlSerializer xs = new XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, privateKey);
            return sw.ToString();
        }

        public (SigningCredentials, string) GetAudienceSigningKey()
        {
            rsa.FromXmlString(GetPrivateKeyString());
            return (new SigningCredentials(
                key: new RsaSecurityKey(rsa),
                algorithm: SecurityAlgorithms.RsaSha256), GetPublicKeyString());
        }

        public void Dispose()
        {
            rsa?.Dispose();
        }
    }
}